import React from 'react';
import { Switch, Route } from 'react-router-dom';
import asyncComponent from '../../../helpers/AsyncFunc';

class AppRouter extends React.Component {
  render() {
    const { url } = this.props;
    return (
      <Switch>
        <Route
          exact
          path={`${url}/`}
          component={asyncComponent(() => import('../Page/dashboard/dashboard'))}
        />
        <Route
          exact
          path={`${url}/inbox`}
          component={asyncComponent(() => import('../../../containers/Mail'))}
        />
        <Route
          exact
          path={`${url}/mailbox`}
          component={asyncComponent(() => import('../../../containers/Mail'))}
        />
        <Route
          exact
          path={`${url}/calendar`}
          component={asyncComponent(() => import('../../../containers/Calendar/Calendar'))}
        />
        {/* <Route
          exact
          path={`${url}/googlemap`}
          component={asyncComponent(() => import('../../../containers/Map/GoogleMap/googleMap'))}
        /> */}
        <Route
          exact
          path={`${url}/leafletmap`}
          component={asyncComponent(() => import('../../../containers/Map/Leaflet/leaflet'))}
        />
        <Route
          exact
          path={`${url}/table_fb`}
          component={asyncComponent(() => import('../../../containers/Tables/fbTables/'))}
        />
        <Route
          exact
          path={`${url}/table_ant`}
          component={asyncComponent(() => import('../../../containers/Tables/antTables'))}
        />
        <Route
          exact
          path={`${url}/allFormComponent`}
          component={asyncComponent(() => import('../../../containers/Forms/allComponents/index'))}
        />
        <Route
          exact
          path={`${url}/InputField`}
          component={asyncComponent(() => import('../../../containers/Forms/Input'))}
        />
        <Route
          exact
          path={`${url}/editor`}
          component={asyncComponent(() => import('../../../containers/Forms/editor/index'))}
        />
        <Route
          exact
          path={`${url}/stepperForms`}
          component={asyncComponent(() => import('../../../containers/Forms/StepperForms'))}
        />
        {/* <Route
          exact
          path={`${url}/FormsWithValidation`}
          component={asyncComponent(() =>
            import('../contianers/Forms/FormsWithValidation')
          )}
        />
        <Route
          exact
          path={`${url}/progress`}
          component={asyncComponent(() => import('../contianers/Forms/Progress'))}
        />
        <Route
          exact
          path={`${url}/ladda_button`}
          component={asyncComponent(() => import('../contianers/Forms/LaddaButton'))}
        />
        <Route
          exact
          path={`${url}/button`}
          component={asyncComponent(() => import('../contianers/Forms/Button'))}
        />
        <Route
          exact
          path={`${url}/tab`}
          component={asyncComponent(() => import('../contianers/Forms/Tab'))}
        />
        <Route
          exact
          path={`${url}/autocomplete`}
          component={asyncComponent(() => import('../contianers/Forms/AutoComplete'))}
        />
        <Route
          exact
          path={`${url}/checkbox`}
          component={asyncComponent(() => import('../contianers/Forms/Checkbox'))}
        />
        <Route
          exact
          path={`${url}/radiobox`}
          component={asyncComponent(() => import('../contianers/Forms/Radiobox/'))}
        />
        <Route
          exact
          path={`${url}/transfer`}
          component={asyncComponent(() => import('../contianers/Forms/Transfer/'))}
        />
        <Route
          exact
          path={`${url}/gridLayout`}
          component={asyncComponent(() => import('../contianers/contianers/Box/GridLayout'))}
        />
        <Route
          exact
          path={`${url}/notes`}
          component={asyncComponent(() => import('../contianers/Notes'))}
        />
        <Route
          exact
          path={`${url}/todo`}
          component={asyncComponent(() => import('../contianers/Todo'))}
        />
        <Route
          exact
          path={`${url}/contacts`}
          component={asyncComponent(() => import('../contianers/Contacts'))}
        />
        <Route
          exact
          path={`${url}/alert`}
          component={asyncComponent(() => import('../contianers/Feedback/Alert'))}
        />
        <Route
          exact
          path={`${url}/modal`}
          component={asyncComponent(() => import('../contianers/Feedback/Modal/'))}
        />
        <Route
          exact
          path={`${url}/message`}
          component={asyncComponent(() => import('../contianers/Feedback/Message'))}
        />
        <Route
          exact
          path={`${url}/notification`}
          component={asyncComponent(() => import('../contianers/Feedback/Notification'))}
        />
        <Route
          exact
          path={`${url}/progress`}
          component={asyncComponent(() => import('../contianers/Feedback/Progress'))}
        />
        <Route
          exact
          path={`${url}/Popconfirm`}
          component={asyncComponent(() => import('../contianers/Feedback/Popconfirm'))}
        />
        <Route
          exact
          path={`${url}/spin`}
          component={asyncComponent(() => import('../contianers/Feedback/Spin'))}
        />
        <Route
          exact
          path={`${url}/shuffle`}
          component={asyncComponent(() => import('../contianers/Shuffle'))}
        />
        <Route
          exact
          path={`${url}/affix`}
          component={asyncComponent(() => import('../contianers/Navigation/affix'))}
        />
        <Route
          exact
          path={`${url}/breadcrumb`}
          component={asyncComponent(() =>
            import('../contianers/Uielements/Breadcrumb/breadcrumb')
          )}
        />
        <Route
          exact
          path={`${url}/backToTop`}
          component={asyncComponent(() => import('../contianers/Navigation/backToTop'))}
        />
        <Route
          exact
          path={`${url}/dropdown`}
          component={asyncComponent(() =>
            import('../contianers/Uielements/Dropdown/dropdown')
          )}
        />
        <Route
          exact
          path={`${url}/op_badge`}
          component={asyncComponent(() => import('../contianers/Uielements/Badge'))}
        />
        <Route
          exact
          path={`${url}/op_card`}
          component={asyncComponent(() => import('../contianers/Uielements/Card'))}
        />
        <Route
          exact
          path={`${url}/op_carousel`}
          component={asyncComponent(() => import('../contianers/Uielements/Carousel'))}
        />
        <Route
          exact
          path={`${url}/op_collapse`}
          component={asyncComponent(() => import('../contianers/Uielements/Collapse'))}
        />
        <Route
          exact
          path={`${url}/op_tooltip`}
          component={asyncComponent(() => import('../contianers/Uielements/Tooltip/'))}
        />
        <Route
          exact
          path={`${url}/rating`}
          component={asyncComponent(() => import('../contianers/Uielements/rating/'))}
        />
        <Route
          exact
          path={`${url}/tree`}
          component={asyncComponent(() => import('../contianers/Uielements/Tree/'))}
        />
        <Route
          exact
          path={`${url}/op_tag`}
          component={asyncComponent(() => import('../contianers/Uielements/Tag'))}
        /> */}
        {/* <Route
          exact
          path={`${url}/op_timeline`}
          component={asyncComponent(() => import('../Uielements/Timeline'))}
        />
        <Route
          exact
          path={`${url}/op_popover`}
          component={asyncComponent(() => import('../Uielements/Popover'))}
        />
        <Route
          exact
          path={`${url}/googleChart`}
          component={asyncComponent(() => import('../Charts/googleChart'))}
        />
        <Route
          exact
          path={`${url}/reecharts`}
          component={asyncComponent(() => import('../Charts/recharts'))}
        />
        <Route
          exact
          path={`${url}/reactVis`}
          component={asyncComponent(() => import('../Charts/reactVis'))}
        />
        <Route
          exact
          path={`${url}/menu`}
          component={asyncComponent(() => import('../Navigation/menu'))}
        />
        <Route
          exact
          path={`${url}/ReactChart2`}
          component={asyncComponent(() => import('../Charts/reactChart2'))}
        />
        <Route
          exact
          path={`${url}/pagination`}
          component={asyncComponent(() =>
            import('../Uielements/Pagination/pagination')
          )}
        />
        <Route
          exact
          path={`${url}/reactTrend`}
          component={asyncComponent(() => import('../Charts/reactTrend'))}
        />
        <Route
          exact
          path={`${url}/echart`}
          component={asyncComponent(() => import('../Charts/echarts/'))}
        />
        <Route
          exact
          path={`${url}/invoice`}
          component={asyncComponent(() => import('../Page/invoice/invoice'))}
        />
        <Route
          exact
          path={`${url}/card`}
          component={asyncComponent(() => import('../Ecommerce/card'))}
        />
        <Route
          exact
          path={`${url}/cart`}
          component={asyncComponent(() => import('../Ecommerce/cart'))}
        />
        <Route
          exact
          path={`${url}/checkout`}
          component={asyncComponent(() => import('../Ecommerce/checkout'))}
        />
        <Route
          exact
          path={`${url}/shop`}
          component={asyncComponent(() =>
            import('../Ecommerce/algolia/instantSearch')
          )}
        />
        <Route
          exact
          path={`${url}/reactDates`}
          component={asyncComponent(() =>
            import('../contianers/AdvancedUI/ReactDates/reactDates')
          )}
        />
        <Route
          exact
          path={`${url}/codeMirror`}
          component={asyncComponent(() => import('../contianers/AdvancedUI/codeMirror'))}
        />
        <Route
          exact
          path={`${url}/uppy`}
          component={asyncComponent(() => import('../contianers/AdvancedUI/uppy'))}
        />
        <Route
          exact
          path={`${url}/dropzone`}
          component={asyncComponent(() => import('../contianers/AdvancedUI/dropzone'))}
        />
        <Route
          exact
          path={`${url}/youtubeSearch`}
          component={asyncComponent(() => import('../YoutubeSearch'))}
        /> */}
        
      </Switch>
    );
  }
}

export default AppRouter;
