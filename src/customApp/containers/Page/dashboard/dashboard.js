import React, { Component } from 'react';
import { connect } from 'react-redux';
import clone from 'clone'

import { Row, Col,Table } from 'antd';
import fakeData from '../../../../containers/Tables/fakeData';
import basicStyle from '../../../config/basicStyle';
import IsoWidgetsWrapper from '../../../../containers/Widgets/widgets-wrapper';
import IsoWidgetBox from '../../../../containers/Widgets/widget-box';
import CardWidget from '../../../../containers/Widgets/card/card-widgets';
import ProgressWidget from '../../../../containers/Widgets/progress/progress-widget';
import SingleProgressWidget from '../../../../containers/Widgets/progress/progress-single';
import ReportsWidget from '../../../../containers/Widgets/report/report-widget';
import StickerWidget from '../../../../containers/Widgets/sticker/sticker-widget';
import SaleWidget from '../../../../containers/Widgets/sale/sale-widget';
import { DateCell, ImageCell, LinkCell, TextCell } from '../../../../containers/Tables/antTables/helperCells';
//import { TableViews, tableinfos, dataList } from '../Tables/antTables';
// import * as rechartConfigs from '../Charts/recharts/config';
// import { StackedAreaChart } from '../Charts/recharts/charts/';
import IntlMessages from '../../../../components/utility/intlMessages';
const dataList = new fakeData(4);
const tableDataList = clone(dataList);
tableDataList.size = 5;
const renderCell = (object, type, key) => {
    const value = object[key];
    switch (type) {
        case 'ImageCell':
            return ImageCell(value);
        case 'DateCell':
            return DateCell(value);
        case 'LinkCell':
            return LinkCell(value);
        default:
            return TextCell(value);
    }
};
const columns = [
    {
        title: <IntlMessages id="antTable.title.image" />,
        key: 'avatar',
        width: '1%',
        className: 'isoImageCell',
        render: object => renderCell(object, 'ImageCell', 'avatar')
    },
    {
        title: <IntlMessages id="antTable.title.firstName" />,
        key: 'firstName',
        width: 100,
        render: object => renderCell(object, 'TextCell', 'firstName')
    },
    {
        title: <IntlMessages id="antTable.title.lastName" />,
        key: 'lastName',
        width: 100,
        render: object => renderCell(object, 'TextCell', 'lastName')
    },
    {
        title: <IntlMessages id="antTable.title.city" />,
        key: 'city',
        width: 200,
        render: object => renderCell(object, 'TextCell', 'city')
    },
    {
        title: <IntlMessages id="antTable.title.street" />,
        key: 'street',
        width: 200,
        render: object => renderCell(object, 'TextCell', 'street')
    },
    {
        title: <IntlMessages id="antTable.title.email" />,
        key: 'email',
        width: 200,
        render: object => renderCell(object, 'LinkCell', 'email')
    },
    {
        title: <IntlMessages id="antTable.title.dob" />,
        key: 'date',
        width: 200,
        render: object => renderCell(object, 'DateCell', 'date')
    }
];
class Dashboard extends Component {
    render() {
        const { rowStyle, colStyle } = basicStyle;
        const wisgetPageStyle = {
            display: 'flex',
            flexFlow: 'row wrap',
            alignItems: 'flex-start',
            padding: '15px',
            overflow: 'hidden',
        };

        const chartEvents = [
            {
                eventName: 'select',
                callback(Chart) {
                    console.log('Selected ', Chart.chart.getSelection());
                },
            },
        ];

        // const stackConfig = {
        //     ...rechartConfigs.StackedAreaChart,
        //     width: window.innerWidth < 450 ? 300 : 500,
        // };
        return (
            <div style={wisgetPageStyle}>

                <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={24} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Report Widget */}
                            <ReportsWidget
                                //label={<IntlMessages id="widget.reportswidget.label" />}
                                //details={<IntlMessages id="widget.reportswidget.details" />}
                            >
                                <SingleProgressWidget
                                    label={
                                        // <IntlMessages id="widget.singleprogresswidget1.label" />
                                        "Cases"
                                    }
                                    percent={70}
                                    barHeight={7}
                                    status="active"
                                    info={true} // Boolean: true, false
                                />
                                <SingleProgressWidget
                                    label={
                                        // <IntlMessages id="widget.singleprogresswidget2.label" />
                                        "Wallet"
                                    }
                                    percent={80}
                                    barHeight={7}
                                    status="active"
                                    info={true} // Boolean: true, false
                                />
                                <SingleProgressWidget
                                    label={ "About"
                                       // <IntlMessages id="widget.singleprogresswidget3.label" />
                                    }
                                    percent={40}
                                    barHeight={7}
                                    status="active"
                                    info={true} // Boolean: true, false
                                />

                            </ReportsWidget>
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={16} sm={24} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            <IsoWidgetBox>
                                {/* TABLE */}
                                <h3 className="isoWidgetLabel"><span>Name of Firms</span></h3>
                                <div className="isoSimpleTable">
                                    <Table
                                        pagination={false}
                                        columns={columns}
                                        dataSource={dataList.getAll()}
                                    />
                                </div>
                            </IsoWidgetBox>
                        </IsoWidgetsWrapper>
                    </Col>
                </Row>

                <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={
                                    12
                                //<IntlMessages id="widget.stickerwidget1.number" />
                            }
                                // text={<IntlMessages id="widget.stickerwidget1.text" />}
                                text={"Clients"}
                                icon="ion-email-unread"
                                fontColor="#ffffff"
                                bgColor="#7266BA"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={ 65
                                    //<IntlMessages id="widget.stickerwidget1.number" />
                                }
                                //text={<IntlMessages id="widget.stickerwidget2.text" />}
                                text={"Cases"}
                                icon="ion-android-create"
                                fontColor="#ffffff"
                                bgColor="#42A5F6"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={ 45
                                //<IntlMessages id="widget.stickerwidget1.number" />
                                }
                                //text={<IntlMessages id="widget.stickerwidget3.text" />}
                                text={"Attorneys"}
                                icon="ion-chatbubbles"
                                fontColor="#ffffff"
                                bgColor="#7ED320"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={ 45
                                //<IntlMessages id="widget.stickerwidget1.number" />
                                }
                                //text={<IntlMessages id="widget.stickerwidget4.text" />}
                                text={"Researchers"}
                                icon="ion-android-people"
                                fontColor="#ffffff"
                                bgColor="#F75D81"
                            />
                        </IsoWidgetsWrapper>
                    </Col>
                </Row>

                <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sale Widget */}
                            <SaleWidget
                                //label={<IntlMessages id="widget.salewidget1.label" />}
                                label={"Today"}
                                //price={<IntlMessages id="widget.salewidget1.price" />}
                                price={"25th Dec 2018"}
                                details={ "Test"
                                //<IntlMessages id="widget.salewidget1.details" />
                                }
                                fontColor="#F75D81"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sale Widget */}
                            <SaleWidget
                                //label={<IntlMessages id="widget.salewidget2.label" />}
                                label={"Available Funds"}
                                //price={<IntlMessages id="widget.salewidget2.price" />}
                                price={"$2,350"}
                                details={ "Funds"
                                //<IntlMessages id="widget.salewidget2.details" />
                                }
                                fontColor="#F75D81"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sale Widget */}
                            <SaleWidget
                                //label={<IntlMessages id="widget.salewidget3.label" />}
                                label={"Firm Cases"}
                                //price={<IntlMessages id="widget.salewidget3.price" />}
                                price={"126"}
                                details={ "Cases"
                                //<IntlMessages id="widget.salewidget3.details" />
                                }
                                fontColor="#F75D81"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    {/* <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sale Widget *}
                            <SaleWidget
                                label={
                                    //<IntlMessages id="widget.salewidget4.label" />
                                }
                                price={
                                    //<IntlMessages id="widget.salewidget4.price" />
                                }
                                details={
                                //<IntlMessages id="widget.salewidget4.details" />
                                }
                                fontColor="#F75D81"
                            />
                        </IsoWidgetsWrapper>
                    </Col> */}
                </Row>

                <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper gutterBottom={20}>
                            {/* Card Widget */}
                            <CardWidget
                                icon="ion-android-chat"
                                iconcolor="#42A5F5"
                                number={ 45
                                //<IntlMessages id="widget.cardwidget1.number" />
                                }
                                text={ "Android"
                                    //<IntlMessages id="widget.cardwidget1.text" />
                                }
                            />
                        </IsoWidgetsWrapper>

                        <IsoWidgetsWrapper gutterBottom={20}>
                            {/* Card Widget */}
                            <CardWidget
                                icon="ion-music-note"
                                iconcolor="#F75D81"
                                //number={<IntlMessages id="widget.cardwidget2.number" />}
                                number={"$100000"}
                                text={"note"
                                //<IntlMessages id="widget.cardwidget2.text" />
                                }
                            />
                        </IsoWidgetsWrapper>

                        <IsoWidgetsWrapper>
                            {/* Card Widget */}
                            <CardWidget
                                icon="ion-trophy"
                                iconcolor="#FEAC01"
                                number={ 45
                                //<IntlMessages id="widget.cardwidget3.number" />
                                }
                                text={ "Test"
                                //<IntlMessages id="widget.cardwidget3.text" />
                                }
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col md={6} sm={12} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper gutterBottom={20}>
                            {/* Progress Widget */}
                            <ProgressWidget
                                label={ "Progrss"
                                //<IntlMessages id="widget.progresswidget1.label" />
                                }
                                details={ "Progress"
                                    //<IntlMessages id="widget.progresswidget1.details" />
                                }
                                icon="ion-archive"
                                iconcolor="#4482FF"
                                percent={50}
                                barHeight={7}
                                status="active"
                            />
                        </IsoWidgetsWrapper>

                        <IsoWidgetsWrapper gutterBottom={20}>
                            {/* Progress Widget */}
                            <ProgressWidget
                                label={ "Label"
                                    //<IntlMessages id="widget.progresswidget2.label" />
                                }
                                details={ "Detail"
                                    //<IntlMessages id="widget.progresswidget2.details" />
                                }
                                icon="ion-pie-graph"
                                iconcolor="#F75D81"
                                percent={80}
                                barHeight={7}
                                status="active"
                            />
                        </IsoWidgetsWrapper>

                        <IsoWidgetsWrapper>
                            {/* Progress Widget */}
                            <ProgressWidget
                                label={"abel"
                                    //<IntlMessages id="widget.progresswidget3.label" />
                                }
                                details={"details"
                                    //<IntlMessages id="widget.progresswidget3.details" />
                                }
                                icon="ion-android-download"
                                iconcolor="#494982"
                                percent={65}
                                barHeight={7}
                                status="active"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    {/* <Col md={12} sm={24} xs={24} style={colStyle}>
                        <IsoWidgetsWrapper>
                            <IsoWidgetBox height={455}>
                                <StackedAreaChart {...stackConfig} />
                            </IsoWidgetBox>
                        </IsoWidgetsWrapper>
                    </Col> */}
                </Row>






            </div>
        );
    }
}


const mapStateToProps = state => ({
    isLoggedIn: state.Auth.get('idToken') !== null ? true : false,
    username: state.username,
    //tableInfo:

});

const mapDispatchToProps = dispatch => ({
   

});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);