import React, { Component } from 'react';

import Form from '../../components/uielements/form';
import clone from 'clone';
import Input from '../../components/uielements/input';
import InputNumber from '../../components/uielements/InputNumber';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import Checkbox, {
  CheckboxGroup
} from '../../components/uielements/checkbox';

import { Row, Col } from 'antd';
import ContentHolder from '../../components/utility/contentHolder';
import Tabs from '../../components/uielements/tabs';
import Select from '../../components/uielements/select';
import AutoComplete from '../../components/uielements/autocomplete';
import DatePicker from '../../components/uielements/datePicker';
import { TableViews, tableinfos, dataList } from '../Tables/antTables';

import { Icon, message} from 'antd';
import Button, { ButtonGroup } from '../../components/uielements/button';
 import Radio from '../../components/uielements/radio';
 import Menu from '../../components/uielements/menu';
 import IntlMessages from '../../components/utility/intlMessages';
 import Dropdown from '../../components/uielements/dropdown';
import basicStyle from '../../config/basicStyle';


const tableDataList = clone(dataList);
tableDataList.size = 5;
const SubMenu = Menu.SubMenu;

// export default class IsoWidgets extends Component {
//   render() {
//     const { rowStyle, colStyle } = basicStyle;
//     const wisgetPageStyle = {
//       display: 'flex',
//       flexFlow: 'row wrap',
//       alignItems: 'flex-start',
//       padding: '15px',
//       overflow: 'hidden',
//     };

// const FormItem = Form.Item;

// const formItemLayout = {
//   labelCol: {
//     xs: { span: 24 },
//     sm: { span: 5 },
//   },
//   wrapperCol: {
//     xs: { span: 24 },
//     sm: { span: 12 },
//   },
// };
const defaultCheckedList = ['Apple', 'Orange'];
const plainOptions = ['Apple', 'Pear', 'Orange'];


export default class Cases extends Component {
    state = {
        size: 'default',
        loading: false,
        iconLoading: false,
        dataSource: [],
        visible: false,
        checkedList: defaultCheckedList,
        indeterminate: true,
        checkAll: false
        
      };

    
      
      handleOnChange = checkedValues => {
        console.log('checked = ', checkedValues);
      };

      onChange = checkedList => {
        this.setState({
          checkedList,
          indeterminate:
            !!checkedList.length && checkedList.length < plainOptions.length,
          checkAll: checkedList.length === plainOptions.length
        });
      };
      onCheckAllChange = e => {
        this.setState({
          checkedList: e.target.checked ? plainOptions : [],
          indeterminate: false,
          checkAll: e.target.checked
        });
      };

      handleChange = value => {
        this.setState({
          dataSource:
            !value || value.indexOf('@') >= 0
              ? []
              : [`${value}@gmail.com`, `${value}@163.com`, `${value}@qq.com`],
        });
      };
      handleButtonClick = e => {
        message.info('Click on left button.');
        console.log('click left button', e);
      };
    
      handleMenuClickToLink = e => {
        message.info('Click on menu item.');
        console.log('click', e);
      };
    
      handleMenuClick = e => {
        if (e.key === '3') {
          this.setState({ visible: false });
        }
      };
      handleVisibleChange = flag => {
        this.setState({ visible: flag });
      };
  render() {
    const plainOptions = ['Auto', 'Homeowner', 'Ambrella','Asset Search'];
    const options = [
      { label: 'Auto', value: 'Auto' },
      { label: 'Homeowner', value: 'Homeowner' },
      { label: 'Ambrella', value: 'Ambrella' },
      { label: 'Asset Search', value: 'Asset Search'}
    ];
    const optionsWithDisabled = [
      { label: 'Apple', value: 'Apple' },
      { label: 'Pear', value: 'Pear' },
      { label: 'Orange', value: 'Orange', disabled: false }
    ];
    
    const { rowStyle, colStyle, gutter } = basicStyle;

    const InputGroup = Input.Group;
    const Option = Select.Option;
    const Search = Input.Search;
    const selectBefore = (
      <Select defaultValue="Http://" style={{ width: 80 }}>
        <Option value="Http://">Http://</Option>
        <Option value="Https://">Https://</Option>
      </Select>
    );
    const selectAfter = (
      <Select defaultValue=".com" style={{ width: 70 }}>
        <Option value=".com">.com</Option>
        <Option value=".jp">.jp</Option>
        <Option value=".cn">.cn</Option>
        <Option value=".org">.org</Option>
      </Select>
    );

    const demoStyle = {
      marginBottom: '8px',
      marginRight: '8px',
    };
    const menuHover = (
      <Menu>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://redq.io/">
            1st menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://redq.io/">
            2nd menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://redq.io/">
            3d menu item
          </a>
        </Menu.Item>
      </Menu>
    );
    const menuHoverDisable = (
      <Menu>
        <Menu.Item key="0">
          <a target="_blank" rel="noopener noreferrer" href="http://redq.io/">
            1st menu item
          </a>
        </Menu.Item>
        <Menu.Item key="1">
          <a target="_blank" rel="noopener noreferrer" href="http://redq.io/">
            2nd menu item
          </a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3" disabled>3d menu item（disabled）</Menu.Item>
      </Menu>
    );
    const menuClicked = (
      <Menu onClick={this.handleMenuClickToLink}>
        <Menu.Item key="1">1st menu item</Menu.Item>
        <Menu.Item key="2">2nd menu item</Menu.Item>
        <Menu.Item key="3">3d menu item</Menu.Item>
      </Menu>
    );

    const menuSubmenu = (
      <Menu>
        <Menu.Item>1st menu item</Menu.Item>
        <Menu.Item>2nd menu item</Menu.Item>
        <SubMenu title="sub menu">
          <Menu.Item>3d menu item</Menu.Item>
          <Menu.Item>4th menu item</Menu.Item>
        </SubMenu>
      </Menu>
    );
    
    const FormItem = Form.Item;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const TabPane = Tabs.TabPane;
    function callback(key) {
      console.log(key);
    }
    
      const wisgetPageStyle = {
      display: 'flex',
      flexFlow: 'row wrap',
      alignItems: 'flex-start',
      padding: '15px',
      overflow: 'hidden',
    };
    const size = this.state.size;
    const margin = { margin: '0 0 8px 0' };
    // const { rowStyle, colStyle, gutter } = basicStyle;
    return (
<LayoutWrapper>
        {/* */}
<Box>
  <Form>

<Tabs defaultActiveKey="1" onChange={callback}>
  <TabPane tab="MY Cases" key="1">
     {/* TABLE */}
     <Row style={rowStyle} gutter={0} justify="start">     
<Col lg={24} md={12} sm={24} xs={24} style={colStyle}>
   <h3 className="isoWidgetLabel"><span></span></h3>
      <TableViews.SimpleView
        tableInfo={tableinfos[0]}
        dataList={tableDataList}
      />
      </Col>
      </Row>
  </TabPane>
  <TabPane tab="Add New" key="2">
    <h2>Add Cases</h2>
    <p>&nbsp;</p>
    <Row style={rowStyle} gutter={0} justify="start">     
<Col lg={16} md={12} sm={24} xs={24} style={colStyle}>
  
    <FormItem style={{ marginBottom: '10px' }}>
    <InputGroup compact style={{ marginBottom: '20px' }}>
    <Col span="18">
    <Select defaultValue="Zhejiang">
      <Option value="Zhejiang">Policy Type</Option>
      <Option value="Jiangsu">Policy 1</Option>
    </Select>
    <Input
      style={{ width: '85%' }}
      defaultValue="Policy Holder Name" />
      </Col>
   <Col span="6"><DatePicker style={{  width: "100%" }}/></Col> 
  </InputGroup>
  
  <hr className="line1"/>

  </FormItem>
  <FormItem style={{ marginBottom: '10px' }}>
  <label>Information Nedded <span>Check all that apply.</span></label>
    <InputGroup compact style={{ marginBottom: '10px' }}>
    <Col span="18">
    <CheckboxGroup
        options={plainOptions}
        defaultValue={['Auto']}
        onChange={this.handleOnChange}
      />  
        </Col>
        <Col span="6">
        <Dropdown overlay={menuClicked}>
        <Button style={{  width: "100%" }}>
          Select Time Line <Icon type="down" />
        </Button>
      </Dropdown>
        </Col>
    
  </InputGroup>
  </FormItem>
  <FormItem style={{ marginBottom: '10px' }}>

  <InputGroup size="large" style={{ marginBottom: '10px' }}>
        <Col span="6">
        <DatePicker style={{ width: '100%' }}/>
        </Col>
        <Col span="12">
          <Input defaultValue="Street Address" />
        </Col>
        <Col span="6">
          <Input defaultValue="Phone  Number" />
        </Col>
      </InputGroup>
  </FormItem>
  <FormItem style={{ marginBottom: '10px' }}>
  <label>Vehicle Info</label>
  <InputGroup size="large" style={{ marginBottom: '10px' }}>
        <Col span="6">
        <Input defaultValue="Year" />
        </Col>
        <Col span="6">
          <Input defaultValue="Make" />
        </Col>
        <Col span="6">
          <Input defaultValue="Model" />
        </Col>
        <Col span="6">
          <Input defaultValue="VIN Number" />
        </Col>
      </InputGroup>
  </FormItem>
  <FormItem style={{ marginBottom: '10px' }}>
  <label>Insurance Information</label>
  <InputGroup size="large" style={{ marginBottom: '10px' }}>
        <Col span="6">
        <Input defaultValue="Insurance Company" />
        </Col>
        <Col span="6">
          <Input defaultValue="Policy Number" />
        </Col>
        <Col span="6">
          <Input defaultValue="Claim Number" />
        </Col>
        <Col span="6">
          <Input defaultValue="Adjuster phone" />
        </Col>
      </InputGroup>
  </FormItem>
  
</Col>

   
<Col lg={8} md={12} sm={24} xs={24} style={colStyle} >
<Col lg={23} md={24} sm={24} xs={24} style={colStyle}  >
<Box flexDirection="column" style={{width: "auto"}} className="float-right">
   

<h3>Case preview</h3>
<p>&nbsp;</p>
<hr className="line1"/>

<p>&nbsp;</p>
<p>Ploicy Type: <span>$100.00 </span></p>
<p>Timeline: <span>$100.00 </span></p>
<h4>Information Needed</h4>
<p>Base: <span>$100.00 </span></p>
<p>Umbrella: <span>$100.00 </span></p>
<p>Asset Search: <span>$100.00 </span></p>
<p>&nbsp;</p>




</Box>


<Button type="primary" style={{float: 'right'}} >Submit</Button>

</Col>


</Col>
</Row>
  </TabPane>
</Tabs>

  
  {/*  */}






</Form>
   </Box>     

</LayoutWrapper>
    );
  }
}
