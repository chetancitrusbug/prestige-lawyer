import React from 'react';
import { Link } from 'react-router-dom';
import { Input, Button, Row, Col } from 'antd';
import IntlMessages from '../../components/utility/intlMessages';
import box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';

class ResetPassword extends React.Component {
  render() {
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      
      <Row style={rowStyle} gutter={0} justify="start">     
      <Col lg={16} md={12} sm={24} xs={24} style={colStyle}>
        <div className="isoFormContent">
          

          <div className="isoFormHeadText">
            <h3>
              <label>Refer Someone</label>
            </h3>
            <p>
            <label>Refer Information</label>
            </p>
          </div>

          <div className="isoResetPassForm">
            <div className="isoInputWrapper">
              <Input size="large" type="text" placeholder="Referal Name" />
            </div>
            <div className="isoInputWrapper">
              <Input size="large" type="text" placeholder="Referal Law Firm" />
            </div>
            <div className="isoInputWrapper">
              <Input size="large" type="text" placeholder="Referal Email" />
            </div>
            <div className="isoInputWrapper">
              <Input size="large" type="text" placeholder="Referal Phone" />
            </div>

       
            <div className="isoInputWrapper">
              <Button type="primary">
                <IntlMessages id="page.resetPassSave" />
              </Button>
            </div>
          </div>
        </div>
        </Col>
        </Row>
   
    );
  }
}

export default ResetPassword;
