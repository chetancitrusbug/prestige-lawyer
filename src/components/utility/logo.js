import React from 'react';
import { Link } from 'react-router-dom';
import { siteConfig } from '../../config.js';

// export default function({ collapsed, styling }) {
export default function({ collapsed }) {
  return (
    <div
      className="isoLogoWrapper">
      {collapsed
        ? <div>
            <h3>
              <Link to="/dashboard">
                {/* <i className={siteConfig.siteIcon} /> */}
                <img src={siteConfig.logo} alt="Prestige Lawyer"/>
              </Link>
            </h3>
          </div>
        : <h3>
            <Link to="/dashboard">
              <img src={siteConfig.logo} alt="Prestige Lawyer" />
              {/* {siteConfig.siteName} */}
            </Link>
          </h3>}
    </div>
  );
}
